<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Customer;

class CustomerTest extends TestCase
{
    public function testCustomerFirstName(): void
    {
        $customer = new Customer();
        $customer->setFirstName("Thierno");
        $this->assertTrue($customer->getFirstName() === "Thierno");
    }
    public function testCustomerLastName(): void
    {
        $customer = new Customer();
        $customer->setLastName("Barry");
        $this->assertTrue($customer->getLastName() === "Barry");
    }
}
